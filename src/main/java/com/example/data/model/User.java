package com.example.data.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class User {

    @Id
    @GeneratedValue
    Long id;
    String username;
    String email;

    public User(String username, String email) {
        this.username = username;
        this.email = email;
    }

}
