package com.example.data.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.lang.Nullable;

import com.example.data.model.User;

public interface UserRepository extends JpaRepository<User, String>, UserRepositoryCustom {

    List<User> findByUsernameAndEmail(@Nullable String username, String email);

    @Query("SELECT u FROM User u WHERE u.email = :e")
    List<User> findByNonNullEmail(@Param("e") String email);

    @Query("SELECT u FROM User u WHERE u.email = :e")
    List<User> findByNullableEmail(@Param("e") @Nullable String email);

    @Query("SELECT c FROM User c WHERE (:e is null or c.email = :e)")
    List<User> findByNullableEmailWithHandledNull(@Param("e") @Nullable String email);

}
