package com.example.data.repo;

import java.util.List;

import com.example.data.model.User;

public interface UserRepositoryCustom {

    List<User> findCustom(String email);

}

