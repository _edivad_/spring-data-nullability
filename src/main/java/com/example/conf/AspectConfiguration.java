package com.example.conf;

import java.util.List;
import java.util.Optional;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;

@Configuration
@EnableAspectJAutoProxy
public class AspectConfiguration {

    @Aspect
    @Component
    @RequiredArgsConstructor
    @Slf4j
    public static class RepositoryMethodsHandler {

        private final Optional<List<RepositoryMethodsHandler.Handler>> handlers;

        @Pointcut("execution(public * org.springframework.data.repository.Repository+.*(..))")
        public void allRepositoryMethods() {
        }

        @Around("allRepositoryMethods()")
        public Object handle(ProceedingJoinPoint pjp) throws Throwable {
            RepositoryMethodsHandler.Invocation invocation = invocationOf(pjp);
            handlers.ifPresent(xs -> xs.forEach(x -> x.handle(invocation)));
            return pjp.proceed();
        }

        private RepositoryMethodsHandler.Invocation invocationOf(ProceedingJoinPoint pjp) {
            Signature signature = pjp.getSignature();
            return new RepositoryMethodsHandler.Invocation(pjp.getTarget(),
                signature.getDeclaringType(),
                signature.getName(), pjp.getArgs());
        }

        @FunctionalInterface
        public interface Handler {
            void handle(RepositoryMethodsHandler.Invocation invocation);
        }


        @Value
        public static class Invocation {
            Object target;
            Class<?> type;
            String method;
            Object[] args;
        }

    }

}
