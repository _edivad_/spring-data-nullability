package test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.example.DummyApplication;
import com.example.conf.AspectConfiguration.RepositoryMethodsHandler;
import com.example.data.model.User;
import com.example.data.repo.UserRepository;

import lombok.extern.slf4j.Slf4j;

@SpringBootTest(classes = {
    DummyApplication.class,
    NullabilityTest.HandlersConfiguration.class
})
@Transactional
class NullabilityTest {

    @Configuration
    static class HandlersConfiguration {

        @Component
        @Slf4j
        static class MethodLogger implements RepositoryMethodsHandler.Handler {

            @Override
            public void handle(RepositoryMethodsHandler.Invocation invocation) {
                log.info("invoked method {}", invocation);
            }

        }


        @Component
        @Slf4j
        static class NullParameterMethodLogger implements RepositoryMethodsHandler.Handler {

            @Override
            public void handle(RepositoryMethodsHandler.Invocation invocation) {
                for (Object arg : invocation.getArgs()) {
                    if (arg == null) {
                        log.error("invoked method {}", invocation);
                        return;
                    }
                }
            }

        }

    }


    @Autowired
    UserRepository repo;

    @Test
    void dataSuccessfullySaved() {
        // given
        List<User> users = List.of(
            new User("a", "a@example.com"),
            new User("b", "b@example.com"),
            new User("c", "c@example.com"),
            new User("c", null));
        repo.saveAll(users);

        // when
        List<User> result = repo.findAll();

        // verify
        assertEquals(4, result.size());
    }

    @Test
    void throwsException_whenParameterIsNull() {
        // given
        List<User> users = List.of(
            new User("u", "u@example.com"),
            new User("u", null));
        repo.saveAll(users);

        // when
        IllegalArgumentException e = assertThrows(IllegalArgumentException.class,
            () -> repo.findByUsernameAndEmail("u", null));

        // verify
        assertEquals("Parameter email in UserRepository.findByUsernameAndEmail must not be null!",
            e.getMessage());
    }

    @Test
    void exceptionNotThrown_whenParameterIsNullable() {
        // given
        List<User> users = List.of(
            new User("u", "u@example.com"),
            new User(null, "u@example.com"));
        List<User> savedUsers = repo.saveAll(users);
        User userWithNullUsername = savedUsers.stream()
            .filter(u -> u.getUsername() == null)
            .findFirst()
            .orElseThrow(IllegalStateException::new);

        // when
        List<User> result = repo.findByUsernameAndEmail(null, "u@example.com");

        // verify
        assertEquals(1, result.size());
        assertEquals(userWithNullUsername.getId(), result.get(0).getId());
        assertNull(result.get(0).getUsername());
        assertEquals("u@example.com", result.get(0).getEmail());
    }

    @Test
    void throwsException_whenParameterIsNullEvenUsingCustomQuery() {
        // given
        repo.save(new User("u", null));

        // when
        IllegalArgumentException e = assertThrows(IllegalArgumentException.class,
            () -> repo.findByNonNullEmail(null));

        // verify
        assertEquals("Parameter email in UserRepository.findByNonNullEmail must not be null!",
            e.getMessage());
    }

    @Test
    void exceptionNotThrown_whenParameterIsNullableButNullValueMustBeHandledWithCare() {
        // given
        repo.save(new User("u", null));

        // when
        List<User> result = repo.findByNullableEmail(null);

        // verify
        assertTrue(result.isEmpty());
    }

    @Test
    void exceptionNotThrown_whenParameterIsNullableAndNullValueHandledCorrectly() {
        // given
        repo.save(new User("u", null));

        // when
        List<User> result = repo.findByNullableEmailWithHandledNull(null);

        // verify
        assertEquals(1, result.size());
        assertEquals("u", result.get(0).getUsername());
        assertNull(result.get(0).getEmail());
    }

    @Test
    void throwsException_whenCustomMethodIsInvoked() {
        // given
        repo.save(new User("u", null));

        // when
        IllegalArgumentException e = assertThrows(IllegalArgumentException.class,
            () -> repo.findCustom(null));

        // verify
        assertEquals("Parameter email in UserRepositoryCustom.findCustom must not be null!",
            e.getMessage());
    }

}
